Projeto:
----------------------------------
- BaianorShell

Integrantes:
----------------------------------
- André Vaz Miyagi
- Bruno Anthony Shimura
- Enzo Dal Evedove
- Gabriel Martinez Ramos dos Santos

Oque é:
-----------------------------------
- Sistema de gerenciamento de abastecimento em postos 


Finalidade:
-----------------------------------
- Projeto de Desenvolvimento de Sistemas de Informação


Ferramentas:
-----------------------------------
- Framework: Laravel
- Banco de Dados: Mysql

Apresentação do projeto:
-----------------------------------
- [apresentação final](https://gitlab.com/BrunoAnthonyShimura/baianorshell/-/wikis/uploads/38ff533b468ac0ece82098876a35aa01/apresenta%C3%A7%C3%A3o-final.pdf)

Link do projeto:
-----------------------------------
- [BainorShell](http://baianorshell.herokuapp.com/)

![](https://gitlab.com/BrunoShimura/baianorshell/-/wikis/uploads/d0d880a1b7808a5e6d4b0111f1cd5363/image.png)