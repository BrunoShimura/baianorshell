@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>
            <div class="card">
                <div class="card-header">Gerar QR code</div>
                <form  action="/qrcode-with-image">
                    <div class="card-body">
                        <img src="{!! asset('img/Qrcode.png')  !!} "width="200" height="200">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Posto:</label>
                            <div class="col-md-6">
                                <select class="input-group form-control" name="option">
                                    <option value="" disabled selected>Escolha o posto</option>
                                    <option value="1">Posto Shell</option>
                                    <option value="2">Posto Ipiranga</option>
                                    <option value="3">Posto Girroto</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Valor em R$:</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Litros:</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" >
                                    Confirmar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
