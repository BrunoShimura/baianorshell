@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body" name="oldBalance">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>

        <form>
            <div class="card">
                <div class="card-header">Recarregar com cartão</div>

                <div class="card-body">
                    <img src="{!! asset('img/Cartao.png')  !!} "width="200" height="200">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Valor em R$:</label>

                        <div class="col-md-6">
                            <input type="number" step="0.01" name="balance" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Bandeira do Cartão:</label>
                        <div class="col-md-6">
                            <select class="input-group form-control" name="option">
                                <option value="" disabled selected>Bandeira</option>
                                <option value="1">MasterMasterCard</option>
                                <option value="2">VisaVisa 16 Dígitos</option>
                                <option value="3">American ExpressAmerican Express</option>
                                <option value="4">Diners ClubDiners Club</option>
                                <option value="5">DiscoverDiscover</option>
                                <option value="6">enRouteenRoute</option>
                                <option value="7">JCBJCB</option>
                                <option value="8">VoyagerVoyager</option>
                                <option value="9">HiperCardHiperCard</option>
                                <option value="10">AuraAura</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Número do Cartão (CVV):</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Data de Validade:</label>

                        <div class="col-md-6">
                            <input id="name" type="date" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Código Segurança:</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Confirmar
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <form>
            <div class="card">
                <div class="card-header">Recarregar com boleto</div>

                <div class="card-body">
                    <img src="{!! asset('img/Boleto.png')  !!} "width="200" height="200">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Valor em R$:</label>

                        <div class="col-md-6">
                            <input id="name" type="number" step="0.01"  name="balance" class="form-control">
                        </div>
                    </div>

                    <!-- <div class="spinner-border"></div> -->

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Confirmar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
