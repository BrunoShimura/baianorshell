@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>
            <div class="card">
                <div class="card-header">Extrato</div>
                <div class="card-body">
                    <img src="{!! asset('img/Planilha.png')  !!} "width="200" height="200">
                </div>
                <table class="table">
                    <tr>
                      <th>Data</th>
                      <th>Ação</th>
                      <th>Valor</th>
                    </tr>
                    <tr>
                      <td>04/06/2019</td>
                      <td>Compra</td>
                      <td>- 55,40 R$</td>
                    </tr>
                    <tr>
                      <td>01/07/2020</td>
                      <td>Recarga</td>
                      <td>+ 100,00 R$</td>
                    </tr>
                    <tr>
                      <td>30/08/2019</td>
                      <td>Recarga</td>
                      <td>+ 50,00 R$</td>
                    </tr>
                    <tr>
                        <td>09/09/2019</td>
                        <td>Compra</td>
                        <td>- 55,40 R$</td>
                      </tr>
                      <tr>
                        <td>01/10/2019</td>
                        <td>Recarga</td>
                        <td>+ 100,00 R$</td>
                      </tr>
                      <tr>
                        <td>24/01/2020</td>
                        <td>Compra</td>
                        <td>- 100,00 R$</td>
                      </tr>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection
