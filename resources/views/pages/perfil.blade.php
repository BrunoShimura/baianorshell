@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>
            <div class="card">
                <div class="card-header">Perfil</div>
                <div class="card-body">
                    <img src="/uploads/avatars/{{$user->avatar}}" style="width:150px; height:150px; border-radius:50%">
                    <h2>Perfil de {{$user->name}}</h2>
                    <form   enctype="multipart/form-data" action="/perfil" method="POST">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Nova imagem:</label>
                            <div class="col-md-6">
                                <input class="form-control" type="file" name="avatar">
                                <input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <input type="submit" class="btn-sm" value="Salvar Imagem">
                            </div>
                        </div>

                    </form>
                        <form enctype="multipart/form-data" action="/perfil" method="POST">
                            @csrf
                            <h3> </h3>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nome:</label>

                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="name" value="{{$user->name}}">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Nome inválido</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-mail:</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>E-mail inválido</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Nova Senha:</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Senha inválida</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Senha:</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Atualizar
                                    </button>
                                </div>
                            </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
