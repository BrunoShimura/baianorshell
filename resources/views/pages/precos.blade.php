@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>
            <div class="card">
                <div class="card-header">Preço do Combustivel</div>

                <div class="card-body">
                    <div id="chartContainer1" style="height: 300px; width: 100%;"></div>
                    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Preço do Combustivel</div>

                <div class="card-body">
                    <div id="piechart" style="width: 300px; height: 300px;"></div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection

