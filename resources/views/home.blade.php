@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" >Saldo</div>

                <div class="card-body">
                    {{ Auth::user()->balance }} R$
                </div>
            </div>
            <div class="card" type="button" onclick="location.href='{{ route('extrato') }}'">
                <div class="card-header" >Extrato</div>

                <div class="card-body">
                    <img src="{!! asset('img/Planilha.png')  !!} "width="200" height="200">
                </div>
            </div>
            <div class="card" type="button" onclick="location.href='{{ route('qrcode') }}'">
                <div class="card-header">Gerar QR code</div>

                <div class="card-body">
                    <img src="{!! asset('img/Qrcode.png')  !!} "width="200" height="200">
                </div>
            </div>
            <div class="card" type="button" onclick="location.href='{{ route('recarga') }}'">
                <div class="card-header">Recarga de Saldo</div>

                <div class="card-body">
                    <img src="{!! asset('img/Transferir.png')  !!} "width="200" height="200">
                </div>
            </div>
            <div class="card" type="button" onclick="location.href='{{ route('precos') }}'">
                <div class="card-header">Preço do Combustivel</div>

                <div class="card-body">
                    <img src="{!! asset('img/Grafico.png')  !!} "width="200" height="200">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
