<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{!! asset('img/BaianorShell.png') !!}" />
    <title>BaianorShell</title>

    <!-- Scripts -->
    <script src="{{ asset('js/Chart.js') }}"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <img src="/img/BaianorShell.png" style="width:50px; height:50px; top:10px; left:10px;">
                    BaianorShell
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Entrar</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Cadastrar</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="color:#fcf8e3" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="width:32px; height:32px; top:10px; left:10px; border-radius:50%">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>
                                    <a class="dropdown-item" href="{{ route('perfil') }}">
                                        Perfil
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src='https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js'></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer1", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "GDP Growth Rate - 2016"
            },
            axisY: {
                title: "Growth Rate (in %)",
                suffix: "%",
                includeZero: false
            },
            axisX: {
                title: "Countries"
            },
            data: [{
                type: "column",
                yValueFormatString: "#,##0.0#\"%\"",
                dataPoints: [
                    { label: "India", y: 7.1 },
                    { label: "China", y: 6.70 },
                    { label: "Indonesia", y: 5.00 },
                    { label: "Australia", y: 2.50 },
                    { label: "Mexico", y: 2.30 },
                    { label: "UK", y: 1.80 },
                    { label: "United States", y: 1.60 },
                    { label: "Japan", y: 1.60 }

                ]
            }]
        });
        chart.render();

        }


        </script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load("current", {packages:["corechart"]});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Language', 'Speakers (in millions)'],
              ['Assamese', 13], ['Bengali', 83], ['Bodo', 1.4],
              ['Dogri', 2.3], ['Gujarati', 46], ['Hindi', 300],
              ['Kannada', 38], ['Kashmiri', 5.5], ['Konkani', 5],
              ['Maithili', 20], ['Malayalam', 33], ['Manipuri', 1.5],
              ['Marathi', 72], ['Nepali', 2.9], ['Oriya', 33],
              ['Punjabi', 29], ['Sanskrit', 0.01], ['Santhali', 6.5],
              ['Sindhi', 2.5], ['Tamil', 61], ['Telugu', 74], ['Urdu', 52]
            ]);

            var options = {
              title: 'Indian Language Use',
              legend: 'none',
              pieSliceText: 'label',
              slices: {  4: {offset: 0.2},
                        12: {offset: 0.3},
                        14: {offset: 0.4},
                        15: {offset: 0.5},
              },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
          }
        </script>

    @yield('scripts')
</body>
</html>
