<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/extrato', 'ExtratoController@index')->name('extrato');

Route::get('/perfil', 'PerfilController@index')->name('perfil');
Route::post('/perfil', 'PerfilController@update')->name('update');

Route::get('/qrcode', 'QrcodeController@index')->name('qrcode');

Route::get('/recarga', 'RecargaController@index')->name('recarga');

Route::get('/precos', 'PrecosController@index')->name('precos');

Route::get('qrcode-with-image', function () {
    $image = \QrCode::format('png')
                    ->merge('img/BaianorShell.png', 0.2, true)
                    ->size(500)->errorCorrection('H')
                    ->generate('Bem vindo ao BaianorShell');
 return response($image)->header('Content-type','image/png');
});
