<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->float('gasoline')->nullable()->default(0.00);
            $table->float('diesel')->nullable()->default(0.00);
            $table->float('alcohol')->nullable()->default(0.00);
            $table->string('avatar')->default('default.jpg');
            $table->string('address')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
