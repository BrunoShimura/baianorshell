<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Station;

class StationController extends Controller
{
    public function index()
    {
        return Station::all();
    }
    public function store(Request $request)
    {
        Station::create($request->all());
    }
    public function show($id)
    {
        return Station::findOrFail($id);
    }
    public function update(Request $request, $id)
    {
        $station = Station::findOrFail($id);
        $station -> update($request->all());
    }
    public function destroy($id)
    {
        $station = Station::findOrFail($id);
        $station -> delete();
    }
}
