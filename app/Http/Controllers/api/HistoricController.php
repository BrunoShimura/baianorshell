<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Historic;

class HistoricController extends Controller
{
    public function index()
    {
        return Historic::all();
    }
    public function store(Request $request)
    {
        Historic::create($request->all());
    }
    public function show($id)
    {
        return Historic::findOrFail($id);
    }
    public function update(Request $request, $id)
    {
        $historic = Historic::findOrFail($id);
        $historic -> update($request->all());
    }
    public function destroy($id)
    {
        $historic = Historic::findOrFail($id);
        $historic -> delete();
    }
}
