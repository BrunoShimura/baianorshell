<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historic extends Model
{
    protected $fillable = [
        'action', 'value', 'balance','id_users','id_stations',
    ];
}
